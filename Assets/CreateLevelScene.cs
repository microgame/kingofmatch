﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;

public class CreateLevelScene : MonoBehaviour 
{
    [SerializeField]private GridLayoutGroup LevelGrid;
    [SerializeField]private Sprite[] Blocks;
    [SerializeField]private Slider RowCountSlider;
    [SerializeField]private Slider ColumnCountSlider;

    private RectTransform m_LevelGridTransform;
    private int m_SelectBlock;

    private int m_Mode;

    void Awake()
    {
        m_LevelGridTransform = LevelGrid.GetComponent<RectTransform>();
    }

	// Use this for initialization
	void Start () {
        OnButtonResetClick();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void SetLevelGridSize(int Row, int Column)
    {
        // Cell_h * Row + space * (Row - 1) + pad = Grid_h   =>   Cell_h = (Grid_h - pad - space * (Row - 1)) / Row
        float cell_w = (m_LevelGridTransform.rect.width - LevelGrid.padding.horizontal - LevelGrid.spacing.x * (Column - 1)) / Column;
        float cell_h = (m_LevelGridTransform.rect.height - LevelGrid.padding.vertical - LevelGrid.spacing.y * (Row - 1)) / Row;

        LevelGrid.cellSize = new Vector2(cell_w, cell_h);

        ResetLevel();

        int addCount = Row * Column - LevelGrid.transform.childCount;

        //add less
        for(int i = 0; i < addCount; i++)
        {
            var obj = new GameObject();
            var trans = obj.AddComponent<RectTransform>();
            trans.SetParent(LevelGrid.transform, false);
            obj.AddComponent<Image>().sprite = Blocks[0];
            var btn = obj.AddComponent<Button>();
            btn.transition = Selectable.Transition.None;
            btn.navigation = new Navigation() { mode = Navigation.Mode.None };
            btn.onClick.AddListener(delegate() {
                OnButtonLevelGridItemClick(btn);
            });
            #if UNITY_EDITOR
            btn.name = "item_" + 0;
            #endif
        }

        //hide more
        for(int i = addCount; i < 0; i++)
        {
            LevelGrid.transform.GetChild(LevelGrid.transform.childCount + i).gameObject.SetActive(false);
        }
    }

    void ResetLevel()
    {
        foreach(Transform child in LevelGrid.transform)
        {
            var img = child.GetComponent<Image>();
            img.sprite = Blocks[0];
            img.gameObject.SetActive(true);
        }
    }

    string EncodeLevel()
    {
        int maxRows = (int)RowCountSlider.value;
        int maxCols = (int)ColumnCountSlider.value;

        StringBuilder sb = new StringBuilder();
        //Create save string
        sb.AppendFormat("MODE {0}", m_Mode);
        sb.Append("\r\n");
        sb.AppendFormat("SIZE {0}/{1}", maxCols, maxRows);
        sb.Append("\r\n");
        sb.AppendFormat("LIMIT {0}/{1}", 0);
        sb.Append("\r\n");
        sb.AppendFormat("COLOR LIMIT {0}", 6);
        sb.Append("\r\n");
        sb.AppendFormat("STARS {0}/{1}/{2}", 100, 200, 300);
        sb.Append("\r\n");
        sb.AppendFormat("COLLECT COUNT {0}/{1}", 0, 0);
        sb.Append("\r\n");
        if (m_Mode == (int)Target.INGREDIENT)
            sb.AppendFormat("COLLECT ITEMS {0}/{1}", 0, 0);
        else if (m_Mode == (int)Target.COLLECT)
            sb.AppendFormat("COLLECT ITEMS {0}/{1}", 0, 0);
        else
            sb.AppendFormat("COLLECT ITEMS {0}/{1}", 0, 0);


        sb.Append("\r\n");


        //set map data
        for (int row = 0; row < maxRows; row++) {
            for (int col = 0; col < maxCols; col++) {
                sb.AppendFormat("{0}{1}", 1, 0);
                //if this column not yet end of row, add space between them
                if (col < (maxCols - 1))
                    sb.Append(" ");
            }
            //if this row is not yet end of row, add new line symbol between rows
            if (row < (maxRows - 1))
                sb.Append("\r\n");
        }

        return sb.ToString();
    }

    public void OnButtonResetClick()
    {
        SetLevelGridSize((int)RowCountSlider.value, (int)ColumnCountSlider.value);
    }

    public void OnBlockSelectChanged(int index)
    {
        m_SelectBlock = index;
    }

    public void OnButtonLevelGridItemClick(Button btn)
    {
        btn.image.sprite = Blocks[m_SelectBlock];

        #if UNITY_EDITOR
        btn.name = "item_" + m_SelectBlock;
        #endif
    }
}
